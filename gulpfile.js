var del = require('del'),
    gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    postcss = require('gulp-postcss'),
    cssnano = require('cssnano'),
    autoprefixer = require('autoprefixer'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    stylus = require('gulp-stylus'),
    pathWeb = 'assets/';

gulp.task('clean_dist', function(){
    return del([pathWeb + '_dist']).then(paths => {
        console.log('\n\x1b[46m\x1b[30m Pasta _dist deletada \x1b[0m\n');
    });
});

gulp.task('copy_fonts', function(){
    return gulp.src(pathWeb + '_src/fonts/**/*')
   .pipe(gulp.dest(pathWeb + '_dist/fonts'));
});


gulp.task('styles', function(){
  return gulp.src([pathWeb + '_src/css/main.styl'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(stylus())
    .pipe(postcss([
        autoprefixer({ browsers: ['last 2 versions'] }),
    ]))
    .pipe(gulp.dest(pathWeb + '_dist/css/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(postcss([
        cssnano()
    ]))
    .pipe(gulp.dest(pathWeb + '_dist/css/'));
});

gulp.task('scripts', function(){
    return gulp.src(pathWeb + '_src/js/**/*.js')
      .pipe(plumber({
        errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(gulp.dest(pathWeb + '_dist/js/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest(pathWeb + '_dist/js/'))
});

gulp.task('watch_all', function() {
  gulp.watch(pathWeb + "_src/css/**/*.styl").on('change', gulp.series("styles", function(){})).on('unlink', gulp.series("styles", function(){}));;
  gulp.watch(pathWeb + "_src/js/**/*.js").on('change', gulp.series("scripts", function(){})).on('unlink', gulp.series("scripts", function(){}));;;
  return true;
})

gulp.task('default', gulp.series("styles", "scripts", "copy_fonts", "watch_all", function(){
    console.log('\n\n\n\n\x1b[46m\x1b[30m Arquivos Compilados! Rodando o Watch! \x1b[0m\n\n\n\n');
}));
