
$(document).ready(function(){
    // Template Functions
    var header = $('#header');
    function compactHeader() {
        if ($(window).scrollTop() > header.outerHeight()) {
            header.addClass('compact');
            $('body').addClass('compact');

        } else {
            header.removeClass('compact');
            $('body').removeClass('compact');
        }
    }

    var $animation_elements = $('.animation-element');
	var $window = $(window);

	function check_if_in_view(isFirstTime) {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);
	 
        $.each($animation_elements, function() {
            var $element = $(this);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var element_bottom_position = (element_top_position + element_height);
        
            //check to see if this current container is within viewport
            if ((element_bottom_position >= window_top_position) &&
                (element_top_position <= window_bottom_position)) {
            $element.addClass('in-view');
            } else {
                if(isFirstTime) $element.removeClass('in-view');
            }
        });
	}

	check_if_in_view(true);
	
	$window.on('scroll resize', function(){
		check_if_in_view(false);
	});

    // Compactando o Meny superior no scroll
	$(window).scroll(function () {
		compactHeader();
    });

    if($(".btn-banner a").length) {
        $(".btn-banner a").click(function( event ) {
            var sectionName = $(this).attr("href");
            $("html, body").animate({ scrollTop: $($(sectionName)).offset().top - 30 }, 500);
        });
    }

    if($(".link-ancor").length) {
        $(".link-ancor").click(function( event ) {
            var sectionName = $(this).attr("href");
            $("html, body").animate({ scrollTop: $($(sectionName)).offset().top - 30 }, 500);
        });
    }

    $('#hme_mid_btn_informacaonutricional').on('click', function(){
        $('.nutri-table').toggle(400);
        return false;
    });

    $(".fancybox").fancybox();

    function openModal(){
        $( '[data-modal]' ).unbind('click').on('click' , function(e){
            e.preventDefault();
            var openModal = $(this).attr("data-modal");
            // $('.modal-default').removeClass('open-modal');
            $('#'+openModal).addClass('open-modal');
        });
    }

    function closeModal(){
        $( '.close-modal' ).unbind('click').on('click' , function(e){
            e.preventDefault();
            
            if($(this).parent().parent().hasClass('open-modal')){
                $(this).parent().parent().removeClass('open-modal');
            }
        });
    }

    openModal();
    closeModal();



});